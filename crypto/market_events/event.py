""" in charge of detecting market events, like reaching a performance or volume threshold. 
list of message senders will be attached to every particular event. 
When the event happens (its underlying condition is satisfied, like a volume reaching a threshold for instance), a
notification message is sent through the message senders."""